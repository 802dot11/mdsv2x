NATIVE = $(shell uname --machine)
BOARD ?= $(NATIVE)
V2XATK ?= 0
V2XLLC ?= 1
POSIX ?= 0

APP = mds1609

# Comment/uncomment the following line to disable/enable debugging
DEBUG := y
ifeq ($(DEBUG),y)
  # "-O" is needed to expand inlines
  EXTRA_CFLAGS += -O -ggdb -DDEBUG -DDEBUG_PRINT_TIMES
else
  EXTRA_CFLAGS += -O2
endif

STACKDIR := $(realpath $(CURDIR)/../..)
SRCDIR := $(realpath $(CURDIR))
OUTDIR := $(realpath $(CURDIR))/$(BOARD)

#############################################################################
# BOARD specific setup
# - Folder locations, CROSSCOMPILE, CCOPTS, INSTALLDIR etc.
#############################################################################

ifneq (,$(findstring $(BOARD),mk5 albatross bach bm1 rdn9000))
  # MK5 etc.
  ARCH := arm
  CROSS_COMPILE ?= ccache arm-linux-gnueabihf-
  INSTALLDIR ?= $(STACKDIR)/../bsp/image/rootfs

  SEC_V2XLIBS ?= libviicsec.so libviicsec.so.1 \
                 libc2x.so libc2x.so.1 libc2x.so.1.0 \
                 libNxDispatcher.so libNxDispatcher.so.1.0 libNxDispatcher.so.1.1 libNxDispatcher.so.1.1.2
  SEC_LDLIBS  ?= -lviicsec -lc2x -Wl,--no-as-needed -lNxDispatcher -ldl

else ifneq (,$(findstring $(BOARD),mq5))
  ARCH := arm
  QNX_BASE ?= /opt/qnx660
  CROSS_COMPILE := $(CCACHE) arm-unknown-nto-qnx6.6.0eabi-
  INSTALLDIR ?=

  CPPFLAGS += -DNO_SYSINFO
  CFLAGS += -fpic

  SEC_V2XLIBS ?=
  SEC_LDLIBS  ?=
  POSIX := 1
  V2XATK := 0
  V2XLLC := 1

else ifneq (,$(findstring $(BOARD),craton2))
  ARCH := arm
  CROSS_COMPILE ?= ccache arm-poky-linux-gnueabi-
  INSTALLDIR ?=
  SDKDIR ?= $(CURDIR)/../../../bsp/atk/craton2-sdk

  CCOPTS += -mfloat-abi=hard -mthumb -mfpu=neon --sysroot=/opt/autotalks/gcc/arm/poky-st/2.0.1/sysroots/cortexa7hf-vfp-neon-poky-linux-gnueabi
  CFLAGS += -I$(SDKDIR)/include
  LDFLAGS += -L$(SDKDIR)/lib/ --sysroot=/opt/autotalks/gcc/arm/poky-st/2.0.1/sysroots/cortexa7hf-vfp-neon-poky-linux-gnueabi

  SEC_V2XLIBS ?= libviicsec.so libviicsec.so.1
  SEC_LDLIBS  ?= -lviicsec
  POSIX := 1
  V2XATK := 1
  V2XLLC := 0

else ifneq (,$(findstring $(BOARD),dg2))
  ifeq (,$(findstring atks,$(CC)))
  $(error Setup the cross compilation environment first: 'source /mnt/atks/atks_yocto_sdk/atks_env_setup.sh')
  endif
  ARCH := arm
  CROSS_COMPILE ?= ccache arm-poky-linux-gnueabi-
  INSTALLDIR ?=
  SDKDIR ?= $(CURDIR)/../../../bsp/atk/craton2-sdk

  CCOPTS += -mfloat-abi=hard -mthumb -mfpu=neon
  CPPFLAGS += -DNO_VIICSEC
  CFLAGS += -I$(SDKDIR)/include
  LDFLAGS  += -L$(SDKDIR)/lib

  SEC_V2XLIBS ?= libviicsec.so libviicsec.so.1 \
                 libaerolinkPKI.so libaerolinkPKI.so.1
  SEC_LDLIBS  ?= -lviicsec \
                 -laerolinkPKI

  POSIX := 1
  V2XATK := 1
  V2XLLC := 0

else
  # Native (PC)
  ARCH ?= $(NATIVE)
  CROSS_COMPILE ?= "ccache "
  INSTALLDIR ?=
  CFLAGS += -Wall -Wextra -Werror

  SEC_V2XLIBS ?= libviicsec.so libviicsec.so.1
  SEC_LDLIBS  ?= -lviicsec
endif

#############################################################################
# File locations
#############################################################################
# Source directories
SRCDIRS := $(SRCDIR)/src \
           $(SRCDIR)/src/can-vstate \
           $(SRCDIR)/src/evw-tx \
           $(SRCDIR)/src/map-tx \
           $(SRCDIR)/src/obe-rx \
           $(SRCDIR)/src/raw-tx \
           $(SRCDIR)/src/rsa-rx \
           $(SRCDIR)/src/rsa-tx \
           $(SRCDIR)/src/spat-tx \
           $(SRCDIR)/src/tc-client \
           $(SRCDIR)/src/tim-generic-rx \
           $(SRCDIR)/src/pos-ext

# Add this list to VPATH, the place $(MAKE) will look for the source files
VPATH := $(SRCDIRS)

# Matching output tree
OUTDIRS := $(subst $(SRCDIR),$(OUTDIR),$(SRCDIRS))

# Include folders
INCDIRS := $(SRCDIRS) \
           $(OUTDIR)/include \
           $(OUTDIR)/include/asn1 \
           $(realpath $(STACKDIR)/../cohda/kernel/include) \
           $(realpath $(STACKDIR)/../ieee1609/kernel/include)

# Aerolink
ifneq (,$(findstring $(BOARD),$(NATIVE) mk5 albatross bach bm1 craton2 rdn9000))
AEROLINK_SRCDIR = $(STACKDIR)/v2x-lib/aerolink
AEROLINK_CONFIG = $(OUTDIR)/aerolink/default
AEROLINK_SCRIPT = $(OUTDIR)/aerolink/rc.aerolink
AEROLINK_TOOLS =  \
		 $(OUTDIR)/aerolink/certadm \
		 $(OUTDIR)/aerolink/ieeeAcfGenerator \
		 $(OUTDIR)/aerolink/acfBuilder \
		 $(OUTDIR)/aerolink/acfInfo
endif

# ffasn1c location/options/etc.
FFASN1DIR    = $(STACKDIR)/../tools/ffasn1c
FFASN1C      = $(FFASN1DIR)/ffasn1c
FFASN1C_OPTS = -fdefine-int-values
FFASN1PY     = $(STACKDIR)/tools/ffasn1.py
FFASN1PREFIX = TODO_
FFASN1DEF    = $(SRCDIR)/src/TODO.asn1
FFASN1OUT    = $(SRCDIR)/src/TODO-asn

#############################################################################
# CPPFLAGS, CFLAGS, LDFLAGS, LDLIBS
#############################################################################
# If using yocto/poky toolchain, use CC in environment
ifneq ($(TARGET_PREFIX),arm-poky-linux-gnueabi-)
  CC := $(shell echo $(CROSS_COMPILE)gcc)
endif
CFLAGS += -DBOARD_$(shell echo $(BOARD) | tr a-z A-Z)
CFLAGS += -fpic -Wall -Wextra -Werror
LDFLAGS += -L$(OUTDIR) \
           -L$(OUTDIR)/lib \
           -Wl,-rpath,./lib

# Generate the include folders' associated CFLAGS entry by adding '-I' before each include folder
CFLAGS += $(foreach DIR, $(INCDIRS), $(addprefix -I, $(DIR)))

ifneq ($(INSTALLDIR),)
CFLAGS += -I$(INSTALLDIR)/usr/include
LDFLAGS += -L$(INSTALLDIR)/usr/lib
endif

# Plugins
V2XLIBS += libCANUDP.so
V2XLIBS += libCongestionControlJ2945.so
#V2XLIBS += libDNPW.so
V2XLIBS += libEEBL.so
#V2XLIBS += libEVW.so
V2XLIBS += libFCW.so
#V2XLIBS += libGLOSA.so
#V2XLIBS += libGMLAN.so
#V2XLIBS += libHLW.so
#V2XLIBS += libICW.so
#V2XLIBS += libLCW.so
#V2XLIBS += libLTA.so
#V2XLIBS += libRSA.so
#V2XLIBS += libSVA.so
#V2XLIBS += libSVW.so
V2XLIBS += libTIMCSW.so
#V2XLIBS += libTIMEvac.so
#V2XLIBS += libTIMGeneric.so
#V2XLIBS += libTIMOversize.so
#V2XLIBS += libTIMSpeed.so
#V2XLIBS += libTIMWorkZone.so

# Cohda libraries (platform, network & facilities layer)
V2XLIBS += libhmi.so libtm.so
LDLIBS += -lhmi -ltm
V2XLIBS += libtc.so libta.so libJudy.a
LDLIBS += -ltc -lta -lJudy
V2XLIBS += libspatmap.so libitsasn.so
LDLIBS += -lspatmap -litsasn
V2XLIBS += libtim.so libldm.so
LDLIBS += -ltim -lldm
V2XLIBS += libext.so
LDLIBS += -lext
V2XLIBS += lib1609fac.so libJ2735.so
LDLIBS += -l1609fac -lJ2735
ifeq ($(POSIX),1)
  CPPFLAGS += -DPOSIX_1609
  V2XLIBS += lib1609net.so
  LDLIBS += -l1609net
else
  V2XLIBS += lib1609.a libnl.so
  LDLIBS += -l1609 -lnl
endif

# to use an external position source (not using libpos), modify pos-ext.c as
# necessary and include that library in this application.
#
# Also remove function calls to libpos initialisation/de-initialisation (e.g.
# GPSRX_LibraryInit, GPSRX_LibraryDeinit

# 'internal' position source with libpos
V2XLIBS += libpos.so libubx.so
LDLIBS += -lpos -lubx

V2XLIBS += libplat.so libclv.so
LDLIBS += -lplat -lclv
V2XLIBS += libconfig.a
LDLIBS += -lconfig
V2XLIBS += libgps.a libpcap.a
LDLIBS += -lgps -lpcap
V2XLIBS += $(SEC_V2XLIBS)
LDLIBS += $(SEC_LDLIBS)
ifeq ($(V2XLLC),1)
  CPPFLAGS += -DV2XLLC
  V2XLIBS += libLLC.so
  LDLIBS += -lLLC
endif
ifeq ($(V2XATK),1)
  CPPFLAGS += -DV2XATK
  V2XLIBS += libatlklocal_linux_u.a
  LDLIBS  += -latlklocal_linux_u -ltomcrypt -ltommath
endif
ifeq ($(BOARD),mq5)
  LDLIBS += -lsocket -lmq -lc -lm
else
  LDLIBS += -lpthread -lrt -ldl -lstdc++ -lm
endif

# v2x-libs are installed locally in $(OUTDIR)/lib
V2XLIBS := $(addprefix $(OUTDIR)/lib/, $(V2XLIBS))

#############################################################################
# SRCS, OBJS, DEPS, LIBS etc.
#############################################################################
# Create a list of *.c sources in SRCDIRS
SRCS = $(foreach DIR,$(SRCDIRS),$(wildcard $(DIR)/*.c))

# Define objects for all sources
OBJS = $(subst $(SRCDIR),$(OUTDIR),$(SRCS:.c=.o))

# Define dependency files for all objects
DEPS = $(OBJS:.o=.d)

#############################################################################
# Rule generation function
#############################################################################
define GenerateRules

$(1)/%.o: %.c $(1)/%.d
	@mkdir -p $$(@D) # Create the required OUTDIR.. folder
	@# Generate dependency information as a side-effect of compilation, not instead of compilation.
	@# -MMD omits system headers from the generated dependencies: if you prefer to preserve system headers as prerequisites, use -MD.
	$$(CC) \
	 -MT $$@ -MMD -MP -MF $(1)/$$*.Td \
	 $$(CCOPTS) $$(CPPFLAGS) $$(CFLAGS) $$(EXTRA_CFLAGS) -c $$< -o $$@
	@# First rename the generated temporary dependency file to the real dependency file.
	@# We do this in a separate step so that failures during the compilation won’t leave a corrupted dependency file.
	mv -f $(1)/$$*.Td $(1)/$$*.d
	@# Second touch the object file; it’s been reported that some versions of GCC may leave the object file older than the dependency file, which causes unnecessary rebuilds.
	touch $$@

# Create a pattern rule with an empty recipe, so that make won’t fail if the dependency file doesn’t yet exist.
$(1)/%.d: ;
# Mark the dependency files precious to make, so they won’t be automatically deleted as intermediate files.
.PRECIOUS: $(1)/%.d

$(1)/%.gpp: %.c asn-gen $(OUTDIR)/include
	@mkdir -p $$(@D) # Create the required OUTDIR.. folder
	$$(CC) -dD -E $$(CCOPTS) $$(CPPFLAGS) $$(CFLAGS) $$(EXTRA_CFLAGS) -c $$< -o $$@

$(1)/%.lst: %.c asn-gen $(OUTDIR)/include
	@mkdir -p $$(@D) # Create the required OUTDIR.. folder
	$$(CC) -S     $$(CCOPTS) $$(CPPFLAGS) $$(CFLAGS) $$(EXTRA_CFLAGS) -c $$< -o $$@

$(1)/%.lint: %.c asn-gen $(OUTDIR)/include
	@mkdir -p $$(@D) # Create the required OUTDIR.. folder
	-splint -shiftimplementation -warnposix -badflag --likelybool $$(CPPFLAGS) $$(CFLAGS) $$< | tee $$@

endef

#############################################################################
# Rules
#############################################################################

all: $(BOARD)

# Generate rules
$(foreach DIR, $(OUTDIRS), $(eval $(call GenerateRules, $(DIR))))

# Include dependencies
-include $(DEPS)

app: version asn-gen $(OUTDIR) ## Build $(APP) binary (Append BOARD=? to specify a non-host target)
	$(MAKE) $(OUTDIR)/$(APP) aerolink

cpp: $(DEPS:.d=.gpp) ## Preprocess all $(APP) sources (Append BOARD=? to specify a non-host target)

lst: $(DEPS:.d=.lst) ## Generate assembler listings for all $(APP) sources (Append BOARD=? to specify a non-host target)

lint: $(DEPS:.d=.lint) ## Generate assembler listings for all $(APP) sources (Append BOARD=? to specify a non-host target)

.PHONY: all app cpp list lint

# Aerolink rule/targets
aerolink: $(V2XLIBS) $(AEROLINK_SCRIPT) $(AEROLINK_TOOLS) $(AEROLINK_CONFIG)

$(AEROLINK_CONFIG) : $(AEROLINK_SRCDIR)/config
	@mkdir -p $@
	@rsync -q -av --exclude="*.svn" $</* $@

$(AEROLINK_SCRIPT) : $(OUTDIR)/aerolink/% : $(AEROLINK_SRCDIR)/%
	@mkdir -p $(dir $@)
	@cp $< $@

$(AEROLINK_TOOLS) : $(OUTDIR)/aerolink/% : $(AEROLINK_SRCDIR)/$(BOARD)/%
	@mkdir -p $(dir $@)
	@cp $< $@

.PHONY: aerolink

# ffasn1c rules
$(FFASN1DEF):
	@ls $(FFASN1DEF) # Nothing to do

$(FFASN1OUT).c: $(OUTDIR) $(FFASN1DEF)
	$(MAKE) --directory $(FFASN1DIR)
	$(FFASN1C) $(FFASN1C_OPTS) -fprefix=$(FFASN1PREFIX) -o $(FFASN1OUT).junk $(FFASN1DEF)
	$(FFASN1PY)                -fprefix=$(FFASN1PREFIX)    $(FFASN1OUT) $(FFASN1DEF)
	# Since we're linking against j2735asn, we don't need to create a link to (and complile) the asn.1 codec files
	#@mkdir -p $(SRCDIR)/src/libffasn1
	#ln -sf $(FFASN1DIR)/src/libffasn1/*.[ch] $(SRCDIR)/libffasn1

asn-gen: ; ## Create all ASN generated code (Nothing yet)

# v2x-lib targets
$(STACKDIR)/v2x-lib/include $(STACKDIR)/v2x-lib/lib/$(BOARD):
	# Build the v2x-libs via the $(STACKDIR)'s makefile
	if [ -e $(STACKDIR)/Makefile ]; then \
	  $(MAKE) -C $(STACKDIR) BOARD=$(BOARD) lib-package POSIX=$(POSIX) ; \
	fi;

$(subst $(OUTDIR)/lib,$(STACKDIR)/v2x-lib/lib/$(BOARD),$(V2XLIBS)): $(STACKDIR)/v2x-lib/lib/$(BOARD)

$(OUTDIR)/include: $(STACKDIR)/v2x-lib/include $(OUTDIR)
	@mkdir -p $@
	@cp -ar $</* $@/

$(V2XLIBS): $(OUTDIR)/lib/%: $(STACKDIR)/v2x-lib/lib/$(BOARD)/% $(OUTDIR)
	@mkdir -p $(dir $@)
	@# The referenced lib*.so library may not exist due to licensing constraints
	@if [ -e $< ]; then \
	  cp -a $< $@ ;\
	fi;

# Build the application
$(OUTDIR)/$(APP): $(OUTDIR)/include $(V2XLIBS) $(OBJS) $(LIBS)
ifeq ($(BOARD),mq5)
	# The QNX linker assumes the presence of '-fpic' means we want a shared library rather than an executable
	$(CC) $(filter-out -fpic,$(CCOPTS) $(CPPFLAGS) $(CFLAGS) $(EXTRA_CFLAGS) $(LDFLAGS) $(OBJS) $(LIBS) $(LDLIBS)) -o $@
else
	$(CC)                    $(CCOPTS) $(CPPFLAGS) $(CFLAGS) $(EXTRA_CFLAGS) $(LDFLAGS) $(OBJS) $(LIBS) $(LDLIBS)  -o $@
endif

# Versioning
SVNVERSION   := $(shell svnversion $(CURDIR))
SVNVERSION   := $(subst :,-,$(SVNVERSION))
SVNVERSION   := $(subst Unversioned directory,Exported,$(SVNVERSION))

$(OUTDIR): # Create a per-target $(OUTDIR)
	@mkdir -p $(OUTDIR)
	@ln -sf $(SRCDIR)/* $(OUTDIR)/ # Link the contents of $(SRCDIR) en-mass to $(OUTDIR)
	@rm -f $(OUTDIR)/$(BOARD) # No circular references allowed
	@rm -f $(OUTDIR)/Makefile # A makefile in $(OUTDIR) is just confusing
	@rm -f $(OUTDIR)/src && mkdir -p $(OUTDIR)/src # Remove the link to src/ and make our own (for build artefacts)

version: $(OUTDIR)
	touch $(OUTDIR)/$@
	-svn info $(CURDIR) > /dev/null 2>&1
	if [ $$? -eq 0 ]; then \
	  echo $(SVNVERSION) > $(OUTDIR)/$@; \
	fi;

.PHONY: version

DESTDIR:=$(shell echo /tmp/$(APP)_$(USER)_$$$$)

# Note the lack of folder prefixes below. TOSHIP files are sourced from $(CURDIR) *and* $(BOARD)
TOSHIP = $(APP) \
         rc.$(APP) \
         obu.cfg obu.conf \
         emergency.cfg emergency.conf \
         rsu.cfg rsu.conf \
         $(wildcard *.conf.$(BOARD)) \
         $(wildcard cfg_convert.py) $(wildcard LibConfigConverter.py) \
         ui \
         lib \
         aerolink \
         post_install \
         version

tarball: app $(BOARD) ## Create a $(APP) tarball. Use `BOARD`-tarball to generate for other targets (e.g. mk5-tarball)
	@rm -f $(APP)-$(BOARD)-*.tgz
	@rm -rf $(DESTDIR)
	@mkdir -p $(DESTDIR)/$(APP)
	@cd $(CURDIR) && rsync -avRL \
	  --exclude="*src*" --exclude="lib*.a" \
	  $(TOSHIP) $(DESTDIR)/$(APP) 2>/dev/null || true
	@cd $(OUTDIR) && rsync -avRL \
	  --exclude="*src*" --exclude="lib*.a" \
	  $(TOSHIP) $(DESTDIR)/$(APP) 2>/dev/null || true
	@sudo chown -R root:root $(DESTDIR)
	@tar -C $(DESTDIR) \
	  --exclude-vcs -czf $(CURDIR)/$(APP)-$(BOARD)-$(SVNVERSION).tgz ./
	@sudo chown $(USER):$(USER) $(CURDIR)/$(APP)-$(BOARD)-$(SVNVERSION).tgz
	@sudo rm -rf $(DESTDIR)
	@echo "== tarball =="
	@du -h $(realpath $(CURDIR))/$(APP)-$(BOARD)-$(SVNVERSION).tgz

%-tarball:
	env BOARD=$* $(MAKE) tarball

.PHONY: tarball,%-tarball

install: $(BOARD)-tarball ## Install $(NATIVE) $(APP) to $(INSTALLDIR)/opt/cohda/application
	sudo install -d $(INSTALLDIR)/opt/cohda/application
ifneq (,$(findstring $(BOARD),mq5))
	# QNX's buildfile doesn't seem to support globbing so temporarily remove the version number when installing
	@mv -f $(CURDIR)/$(APP)-$(BOARD)-$(SVNVERSION).tgz $(CURDIR)/$(APP)-$(BOARD).tgz
	sudo install -c $(CURDIR)/$(APP)-$(BOARD).tgz $(INSTALLDIR)/opt/cohda/application
	@mv -f $(CURDIR)/$(APP)-$(BOARD).tgz $(CURDIR)/$(APP)-$(BOARD)-$(SVNVERSION).tgz
else
	sudo install -c $(CURDIR)/$(APP)-$(BOARD)-$(SVNVERSION).tgz $(INSTALLDIR)/opt/cohda/application
endif

.PHONY: install

clean: ## Clean up. Use `BOARD`-clean to specify the target (e.g. mk5-clean)
	@# Generated source(s)
	@true # Nothing yet
	@# Build tree
	sudo rm -rf $(OUTDIR)
	@# Deployment files & folders
	rm -f $(APP)-$(BOARD)-*.tgz

%-clean:
	env BOARD=$* $(MAKE) clean

.PHONY: clean,%-clean

help:
	@echo -e "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

.PHONY: help

#############################################################################
# $(BOARD) specific rules
#############################################################################

$(NATIVE):
	env BOARD=$@ $(MAKE) app

mk5:
	env BOARD=$@ $(MAKE) app

mq5:
	env BOARD=$@ $(MAKE) app

albatross:
	env BOARD=$@ $(MAKE) app

bach:
	env BOARD=$@ $(MAKE) app

bm1:
	env BOARD=$@ $(MAKE) app

craton2:
	env BOARD=$@ $(MAKE) app

dg2:
	source /mnt/atks/atks_yocto_sdk/atks_env_setup.sh && env BOARD=$@ $(MAKE) app

rdn9000:
	source /opt/fsl-imx-x11/3.14.52-1.1.1/environment-setup-cortexa9hf-vfp-neon-poky-linux-gnueabi && env BOARD=$@ $(MAKE) app

.PHONY: $(NATIVE)
.PHONY: mk5
.PHONY: mq5
.PHONY: albatross
.PHONY: bach
.PHONY: bm1
.PHONY: craton2
.PHONY: dg2
.PHONY: rdn9000
